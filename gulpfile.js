// Include gulp
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var imageResize = require('gulp-image-resize');
var inject = require('gulp-inject-string');
var fs = require('fs');
const pkg = require('./package')
const Url = pkg.defineUrl

gulp.task('makethumbnail', () => {
    return gulp.src('./assets/img/kuchyn_cara/*')
        .pipe(imageResize({
        width : 350,
        crop : false,
        upscale : false
        }))
        .pipe(gulp.dest('./assets/img/thumbnails/kuchyn_cara/'));
})

gulp.task('images', () => {
    return gulp.src('./dist/_nuxt/img/*')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(function(file) { return file.base; }))
    }
);
var replace = require('gulp-replace');
 
gulp.task('replacetext', () => {
  return gulp.src(['./dist/**/*.json', './dist/**/*.js', './dist/**/*.html'])
    .pipe(replace('http://tenerifecarpenter.com/dist/', './'))
    .pipe(gulp.dest(function(file) { return file.base; }))
});
// gulp.task('injectkitchensliderstfile', function(){
//     return gulp.src('./dist/kitchens/index.html')
//         .pipe(inject.before('</body>', '\n<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>\n'))
//         .pipe(gulp.dest(function(file) { return file.base; }))
// });
// gulp.task('injectbodygalleryfile', function(){
//     return gulp.src('./dist/index.html')
//         .pipe(inject.before('</body>', '\n<script src="' + pkg.myurl + 'js/lightgallery.min.js"></script>\n'))
//         .pipe(gulp.dest(function(file) { return file.base; }))
// });
// gulp.task('injectkitchensliderst', function(){
//     //var Urldata = fs.readFileSync(Url + 'gulp/test.js', "utf8");
//     var Urldata = fs.readFileSync(Url + 'gulp/console.js', "utf8");
//     return gulp.src('./dist/kitchens/index.html')
//         //.pipe(inject.before('</body>', '\n<script>'+ Urldata +'</script>\n'))
//         .pipe(inject.before('</body>', '\n<script>'+ Urldata +'</script>\n'))
//         .pipe(gulp.dest(function(file) { return file.base; }))
// });
// gulp.task('injectbodygallery', function(){
//     var Urldata = fs.readFileSync(Url + 'gulp/tiny-slider-define.js', "utf8");
//     return gulp.src('./dist/index.html')
//         .pipe(inject.before('</body>', '\n<script>'+ Urldata +'</script>\n'))
//         .pipe(gulp.dest(function(file) { return file.base; }))
// });
//gulp.task('injectscript', gulp.series('injectkitchensliderstfile', 'injectkitchensliderst', 'injectbodygallery'))
// gulp.task('injectscript', gulp.series('injectkitchensliderstfile', 'injectbodygalleryfile'))