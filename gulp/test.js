var slider = tns({ container: '.my-slider', 
    items: 1, 
    autoplay: true, 
    mouseDrag: true,
    responsive: { 
        640: { items: 2 } 
    } 
}); 